import { UserDto } from "./../../dto/user/user.dto";
import { UserService } from "../../service/user/user.service";
export declare class UserController {
    private userService;
    constructor(userService: UserService);
    create(user: UserDto): UserDto;
    findAll(): UserDto[];
}
