import { UserDto } from "../../dto/user/user.dto";
export declare class UserService {
    users: UserDto[];
    createUser(user: UserDto): UserDto;
    findAllUsers(): UserDto[];
}
