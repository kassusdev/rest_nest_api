import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
<<<<<<< HEAD
import { UpdateResult, DeleteResult } from "typeorm";
=======
>>>>>>> ef9e4d2e38abe234d3a1b5f8fac2bf223bc25860
import { UserDto } from "../../dto/user/user.dto";
import { Repository } from "typeorm";
import { UserEntity } from "../../entity/user/user.entity";

@Injectable()
export class UserService {
  public users: UserDto[] = [];

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>
  ) {}

  createUser(user: UserDto): Promise<UserDto> {
    return this.userRepository.save(user);
  }

  findAllUsers(): Promise<UserDto[]> {
    return this.userRepository.find();
  }
<<<<<<< HEAD

  updateUser(updateUserDto: UserDto) {
    return this.userRepository.update(
      {
        id: updateUserDto.id,
      },
      {
        name: updateUserDto.name,
        lastname: updateUserDto.lastname,
      }
    );
  }

  findOneUser(id: string): Promise<UserDto> {
    return this.userRepository.findOne(id);
  }
  async update(user: UserDto): Promise<UpdateResult> {
    return await this.userRepository.update(user.id, user);
  }
  async delete(id): Promise<DeleteResult> {
    return await this.userRepository.delete(id);
  }
=======
>>>>>>> ef9e4d2e38abe234d3a1b5f8fac2bf223bc25860
}
